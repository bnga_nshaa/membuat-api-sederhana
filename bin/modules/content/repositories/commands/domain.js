const Query = require('../queries/query');
const Command = require('./command');
const wrapper = require('../../../../helpers/utils/wrapper');
const logger = require('../../../../helpers/utils/logger');
const { InternalServerError, ConflictError } = require('../../../../helpers/error');

class Content {
  constructor(db) {
    this.command = new Command(db);
    this.query = new Query(db);
  }

  async postContent(payload) {
    const cfx = 'domain-postContent';
    const { id, title, uploader, rating, total_user, details, material } = payload;
    const content = await this.query.findContent({ id, title, uploader, rating, total_user, details, material });
    if (content.data) {
      logger.log[(cfx, 'content already exist', 'error')];
      return wrapper.error(new ConflictError('content already exist'));
    }

    const { data: result } = await this.command.insertContent(payload);
    if (result.error) {
      logger.log[(cfx, 'content failed to insert', 'error')];
      return wrapper.error(new InternalServerError('content failed to insert'));
    }
    return wrapper.data(result.data);
  }

  async putContent(payload) {
    const cfx = 'domain-updateContent';
    const { id, title, uploader, rating, total_user, details, material } = payload;
    const content = await this.query.findContent({ id, title, uploader, rating, total_user, details, material });
    if (content.data) {
      logger.log[(cfx, 'content already exist', 'error')];
      return wrapper.error(new ConflictError('content already exist'));
    }

    const { data: result } = await this.command.putContent(payload);
    if (result.err) {
      logger.log[(cfx, 'content failed to update', 'error')];
      return wrapper.error(new InternalServerError('content failed to update'));
    }
    return wrapper.data(result.data);
  }
}

module.exports = Content;
