class Command {
  constructor(db) {
    this.db = db;
  }

  async insertContent(document) {
    const { id, title, uploader, rating, total_user, details, material } = document;
    const result = await this.db.prepareQuery('INSERT INTO content (id, title, uploader, rating, total_user, details, material ) VALUES (?, ?, ?, ?, ?, ?, ?)', [
      id,
      title,
      uploader,
      rating,
      total_user,
      details,
      material,
    ]);
    return result;
  }

  async putContent(document) {
    const { id, title, uploader, rating, total_user, details, material } = document;
    const result = await this.db.prepareQuery('UPDATE content set title= ?, uploader = ?, rating = ?,  total_user=?, details=?, material=? WHERE id=?', [
      title,
      uploader,
      rating,
      total_user,
      details,
      material,
      id,
    ]);
    return result;
  }
}

module.exports = Command;
