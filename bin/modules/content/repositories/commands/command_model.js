const joi = require('joi');

const content = joi.object({
  id: joi.number().required(),
  title: joi.string().required(),
  uploader: joi.string().required(),
  rating: joi.number().required(),
  total_user: joi.number().required(),
  details: joi.string().required(),
  material: joi.string().required(),
});

module.exports = {
  content,
};
