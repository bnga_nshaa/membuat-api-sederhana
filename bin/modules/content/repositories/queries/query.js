class Query {
  constructor(db) {
    this.db = db;
  }

  async findContent(parameter) {
    const recordset = await this.db.query('SELECT title, uploader, rating, total_user, details, material FROM content WHERE id = ?', parameter);
    return recordset;
  }
}

module.exports = Query;
