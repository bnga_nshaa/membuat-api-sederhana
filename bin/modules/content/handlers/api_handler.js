const wrapper = require('../../../helpers/utils/wrapper');
const commandHandler = require('../repositories/commands/command_handler');
const commandModel = require('../repositories/commands/command_model');
const queryHandler = require('../repositories/queries/query_handler');
const validator = require('../utils/validator');
const { ERROR: httpError, SUCCESS: http } = require('../../../helpers/http-status/status_code');

const postContent = async (req, res) => {
  const payload = req.body;
  const validatePayload = validator.isValidPayload(payload, commandModel.content);
  const postRequest = async (result) => {
    if (result.err) {
      return result;
    }
    return commandHandler.postContent(result.data);
  };
  const sendResponse = async (result) => {
    /* eslint no-unused-expressions: [2, { allowTernary: true }] */
    result.err ? wrapper.response(res, 'fail', result, 'Insert Content', httpError.CONFLICT) : wrapper.response(res, 'success', result, 'Insert Content', http.OK);
  };
  sendResponse(await postRequest(validatePayload));
};

const putContent = async (req, res) => {
  const payload = req.body;
  const validatePayload = validator.isValidPayload(payload, commandModel.content);
  const putRequest = async (result) => {
    if (result.err) {
      return result;
    }
    return commandHandler.putContent(result.data);
  };
  const sendResponse = async (result) => {
    /* eslint no-unused-expressions: [2, { allowTernary: true }] */
    result.err ? wrapper.response(res, 'fail', result, 'Update Content', httpError.CONFLICT) : wrapper.response(res, 'success', result, 'Update Content', http.OK);
  };
  sendResponse(await putRequest(validatePayload));
};

const getContent = async (req, res) => {
  const { id } = req;
  const getData = async () => queryHandler.getContent(id);
  const sendResponse = async (result) => {
    result.err ? wrapper.response(res, 'fail', result, 'Get Content', httpError.NOT_FOUND) : wrapper.response(res, 'success', result, 'Get Content', http.OK);
  };
  sendResponse(await getData());
};

module.exports = {
  postContent,
  getContent,
  putContent,
};
